//
//  PageScrollViewController.swift
//  ScrollViews
//
//  Created by Shaikh Amjad on 18/12/2015.
//  Copyright © 2015 ShaikhAmjad. All rights reserved.
//
import UIKit

class PageScrollViewController: UIViewController, UIScrollViewDelegate {

    //MARK: Outlets
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!

    //MARK: Properties
    var pageImages: [UIImage] = []
    var pageViews: [UIImageView?] = []

    //Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // 1
        pageImages = [UIImage(named: "photo1.png")!,
            UIImage(named: "photo2.png")!,
            UIImage(named: "photo3.png")!,
            UIImage(named: "photo4.png")!,
            UIImage(named: "photo5.png")!]

        let pageCount = pageImages.count

        // 2
        pageControl.currentPage = 0
        pageControl.numberOfPages = pageCount

        // 3
        for _ in 0..<pageCount {
            pageViews.append(nil)
        }

        // 4
        let pagesScrollViewSize = scrollView.frame.size

        let contentSizeWidth = pagesScrollViewSize.width * CGFloat(pageImages.count)
        let contentSizeHeight = pagesScrollViewSize.height * CGFloat(pageImages.count)

        scrollView.contentSize = CGSize(width: contentSizeWidth, height: contentSizeHeight)
        
        // 5
        loadVisiblePages()
    }

    func loadPage(page: Int) {
        guard page >= 0 && page < pageImages.count else {
            // If it's outside the range of what you have to display, then do nothing
            return
        }

        // 1
        if let _ = pageViews[page] {
            // Do nothing. The view is already loaded.
        } else {
            // 2
            var frame = scrollView.bounds
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0.0

            // 3
            let newPageView = UIImageView(image: pageImages[page])
            newPageView.contentMode = .ScaleAspectFit
            newPageView.frame = frame
            scrollView.addSubview(newPageView)
            
            // 4
            pageViews[page] = newPageView
        }
    }


    func purgePage(page: Int) {
        guard page >= 0 && page < pageImages.count else {
            // If it's outside the range of what you have to display, then do nothing
            return
        }

        // Remove a page from the scroll view and reset the container array
        if let pageView = pageViews[page] {
            pageView.removeFromSuperview()
            pageViews[page] = nil
        }
    }

    func loadVisiblePages() {

        // First, determine which page is currently visible
        let pageWidth = scrollView.frame.size.width
        let pageX = Int(floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))


        // First, determine which page is currently visible
        let pageHeight = scrollView.frame.size.height
        let pageY = Int(floor((scrollView.contentOffset.y + pageHeight) / (pageHeight)))


        let page = pageX > pageY ? pageX : pageY

        // Update the page control
        pageControl.currentPage = page

        // Work out which pages you want to load
        let firstPage = page - 1
        let lastPage = page + 1

        // Purge anything before the first page
        for var index = 0; index < firstPage; ++index {
            purgePage(index)
        }

        // Load pages in our range
        for index in firstPage...lastPage {
            loadPage(index)
        }

        // Purge anything after the last page
        for var index = lastPage+1; index < pageImages.count; ++index {
            purgePage(index)
        }
    }

    func scrollViewDidScroll(scrollView: UIScrollView) {
        // Load the pages that are now on screen
        loadVisiblePages()
    }
    
}
