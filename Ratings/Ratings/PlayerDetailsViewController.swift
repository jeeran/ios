//
//  PlayerDetailsViewController.swift
//  Ratings
//
//  Created by Shaikh Amjad on 25/07/2015.
//  Copyright © 2015 Apple Inc. All rights reserved.
//

import UIKit

class PlayerDetailsViewController: UITableViewController {

    // MARK - Properties
    var player : Player!
    var game:String = "Chess"

    // MARK - Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var doneBarButton: UIBarButtonItem!


    override func viewDidLoad() {
        super.viewDidLoad()
        doneBarButton.enabled = false
    }

    //MARK - Actions
    @IBAction func editing(sender: UITextField) {
        let name = nameTextField.text ?? ""
        doneBarButton.enabled = !(name.isEmpty)

    }

    //MARK - Segue
    @IBAction func selectedGame(segue:UIStoryboardSegue) {
        if let gamePickerViewController = segue.sourceViewController as? GamePickerViewController,
            selectedGame = gamePickerViewController.selectedGame {
                detailLabel.text = selectedGame
                game = selectedGame
        }
    }

    // MARK: - Table view data source
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            nameTextField.becomeFirstResponder()
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SavePlayerDetail" {

            let name = self.nameTextField.text ?? ""
            player = Player(name: name, game: game, rating: 1)
        }
        if segue.identifier == "PickGame" {
            if let gamePickerViewController = segue.destinationViewController as? GamePickerViewController {
                gamePickerViewController.selectedGame = game
            }
        }
    }

    //MARK - Init & Deinit
    required init?(coder aDecoder: NSCoder) {
        print("init PlayerDetailsViewController")
        super.init(coder: aDecoder)
    }

    deinit {
        print("deinit PlayerDetailsViewController")
    }
}
