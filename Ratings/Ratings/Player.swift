//
//  Player.swift
//  Ratings
//
//  Created by Shaikh Amjad on 24/07/2015.
//  Copyright © 2015 Apple Inc. All rights reserved.
//

import UIKit

class Player: NSObject {
  var name: String
  var game: String
  var rating: Int

  init(name: String, game: String, rating: Int) {
    self.name = name
    self.game = game
    self.rating = rating
    super.init()
  }
}