//
//  SampleData.swift
//  Ratings
//
//  Created by Shaikh Amjad on 24/07/2015.
//  Copyright © 2015 Apple Inc. All rights reserved.
//

//Set up sample data

let playersData = [ Player(name:"Bill Evans", game:"Tic-Tac-Toe", rating: 4),
    Player(name: "Oscar Peterson", game: "Spin the Bottle", rating: 5),
    Player(name: "Dave Brubeck", game: "Texas Hold 'em Poker", rating: 2) ]

