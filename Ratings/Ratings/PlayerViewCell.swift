//
//  PlayerViewCell.swift
//  Ratings
//
//  Created by Shaikh Amjad on 24/07/2015.
//  Copyright © 2015 Apple Inc. All rights reserved.
//

import UIKit
class PlayerViewCell: UITableViewCell {
    
    //MARK - Outlets
    @IBOutlet weak var gameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingImageView: UIImageView!

}
