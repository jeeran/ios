//
//  ParkMapViewController.swift
//  Park View
//
//  Created by Niv Yahel on 2014-11-09.
//  Copyright (c) 2014 Chris Wagner. All rights reserved.
//

import MapKit

enum MapType: Int {
  case Standard = 0
  case Hybrid
  case Satellite
}

class ParkMapViewController: UIViewController {

  //MARK: - Properties
  var park = Park(filename: "MagicMountain")
  var selectedOptions = [MapOptionsType]()

  //MARK: - Outlets
  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var mapTypeSegmentedControl: UISegmentedControl!

  //MARK: - Methods
  override func viewDidLoad() {
    super.viewDidLoad()

    let latDelta = park.overlayTopLeftCoordinate.latitude -
      park.overlayBottomRightCoordinate.latitude

    // think of a span as a tv size, measure from one corner to another
    let span = MKCoordinateSpanMake(fabs(latDelta), 0.0)

    let region = MKCoordinateRegionMake(park.midCoordinate, span)

    mapView.region = region
  }

  func loadSelectedOptions() {
    mapView.removeAnnotations(mapView.annotations)
    mapView.removeOverlays(mapView.overlays)

    for option in selectedOptions {
      switch (option) {

      case .MapOverlay:
        addOverlay()
      case .MapPins:
        addAttractionPins()
      default:
        break;
      }
    }
  }

  func addOverlay() {
    //
    let overlay = ParkMapOverlay(park: park)
    mapView.addOverlay(overlay)
  }

  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let optionsViewController = segue.destinationViewController as! MapOptionsViewController
    optionsViewController.selectedOptions = selectedOptions
  }

  func addAttractionPins() {
    let filePath = NSBundle.mainBundle().pathForResource("MagicMountainAttractions", ofType: "plist")
    let attractions = NSArray(contentsOfFile: filePath!)

    for attraction in attractions! {
      let point = CGPointFromString(attraction["location"] as! String)
      let coordinate = CLLocationCoordinate2DMake(CLLocationDegrees(point.x), CLLocationDegrees(point.y))

      let title = attraction["name"] as! String
      let typeRawValue = Int((attraction["type"] as! String))!

      let type = AttractionType(rawValue: typeRawValue)!
      let subtitle = attraction["subtitle"] as! String

      let annotation = AttractionAnnotation(coordinate: coordinate, title: title, subtitle: subtitle, type: type)
      mapView.addAnnotation(annotation)
    }
  }

  // MARK: - Actions
  @IBAction func closeOptions(exitSegue: UIStoryboardSegue) {
    let optionsViewController = exitSegue.sourceViewController as! MapOptionsViewController

    selectedOptions = optionsViewController.selectedOptions
    self.loadSelectedOptions()
  }

  @IBAction func mapTypeChanged(sender: AnyObject) {
    let mapType = MapType(rawValue: mapTypeSegmentedControl.selectedSegmentIndex)
    switch (mapType!) {
    case .Standard:
      mapView.mapType = MKMapType.Standard
    case .Hybrid:
      mapView.mapType = MKMapType.Hybrid
    case .Satellite:
      mapView.mapType = MKMapType.Satellite
    }
  }
}



// MARK: - Map View delegate

extension ParkMapViewController: MKMapViewDelegate {

  func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {

    if overlay is ParkMapOverlay {
      let magicMountainImage = UIImage(named: "overlay_park")
      let overlayView = ParkMapOverlayView(overlay: overlay, overlayImage: magicMountainImage!)

      return overlayView
    }

    return MKOverlayRenderer()
  }

  func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {

    if annotation is AttractionAnnotation {
      let annotationView = AttractionAnnotationView(annotation: annotation, reuseIdentifier: "Attraction")
      annotationView.canShowCallout = true
      return annotationView
    }

    return nil
  }
}
