//
//  FlickrPhotoHeaderView.swift
//  FlickrSearch
//
//  Created by Shaikh Amjad on 22/12/2015.
//  Copyright © 2015 ShaikhAmjad. All rights reserved.
//

import UIKit

class FlickrPhotoHeaderView: UICollectionReusableView {

    //MARK: Outlets
    @IBOutlet weak var label: UILabel!


}

//MARK: Delegates
