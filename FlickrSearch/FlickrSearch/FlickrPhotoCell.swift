//
//  FlickrPhotoCell.swift
//  FlickrSearch
//
//  Created by Shaikh Amjad on 20/12/2015.
//  Copyright © 2015 ShaikhAmjad. All rights reserved.
//

import UIKit

class FlickrPhotoCell: UICollectionViewCell {

    //MARK: Outlest
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selected = false
    }

    override var selected : Bool {
        didSet {
            //self.backgroundColor = selected ? themeColor : UIColor.blackColor()
            self.backgroundColor = UIColor.blackColor()
        }
    }
}
