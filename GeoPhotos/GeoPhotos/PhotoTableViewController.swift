//
//  PhotoTableViewController.swift
//  GeoPhotos
//
//  Created by Shaikh Amjad on 14/01/2016.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit

class PhotoTableViewController: UITableViewController {

  //MARK: Properties
  var photos:[Photo]

  //MARK: Overrides
  required init?(coder aDecoder: NSCoder) {
    photos = []
    super.init(coder: aDecoder)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = false

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem()

    loadPhotos()
  }

  //MARK: Methods
  func loadPhotos() {

    guard let image =  UIImage(named: "photo") else {
      return
    }

    let photo = Photo(name: "photo"
      , photo: image, rating: 3, latitude: 48.0, longitude: 73.10)
    photos += [photo]
  }

  //MARK: UITableViewDataSource
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }

  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return photos.count
  }

  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath)
    -> UITableViewCell {

      let cellIdentifier = "PhotoTableViewCell"
      let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier
        , forIndexPath: indexPath) as! PhotoTableViewCell

      cell.photoImageView.image = photos[indexPath.row].photo
      cell.nameLabel.text = photos[indexPath.row].name
      cell.ratingControl.rating = photos[indexPath.row].rating
      
      return cell

  }

  //MARK: Navigation
  @IBAction func unwindToPhotoList(sender: UIStoryboardSegue) {
    guard let source = sender.sourceViewController as? PhotoViewController
    , photo = source.photo else {
      return
    }

    //Add a new photo
    let newIndexPath = NSIndexPath(forRow: photos.count, inSection: 0)
    photos.append(photo)
    tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Bottom)

  }
}
