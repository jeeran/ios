//
//  GeoTableViewCell.swift
//  GeoPhotos
//
//  Created by Shaikh Amjad on 14/01/2016.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {

  //MARK: Properties

  @IBOutlet weak var photoImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var ratingControl: RatingControl!

  //MARK: Overrides
  override func awakeFromNib() {
    super.awakeFromNib()
  }
}
