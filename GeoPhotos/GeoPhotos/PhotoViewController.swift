//
//  ViewController.swift
//  GeoPhotos
//
//  Created by Shaikh Amjad on 13/01/2016.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit
import Photos

class PhotoViewController: UIViewController
  , UIImagePickerControllerDelegate, UINavigationControllerDelegate {

  //MARK: Properties
  var photo: Photo?
  var name: String
  var latitude: NSNumber
  var longitude: NSNumber

  //MARK: Init
  required init?(coder aDecoder: NSCoder) {
    name = String()
    latitude = NSNumber()
    longitude = NSNumber()

    super.init(coder:aDecoder)
  }

  //MARK: Outlets
  @IBOutlet weak var photoImageView: UIImageView!
  @IBOutlet weak var saveButton: UIBarButtonItem!
  @IBOutlet weak var ratingControl: RatingControl!

  //MARK: Overrides
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


  // MARK: UIImagePickerControllerDelegate
  func imagePickerControllerDidCancel(picker: UIImagePickerController) {
    // Dismiss the picker if the user canceled.
    dismissViewControllerAnimated(true, completion: nil)
  }

  func imagePickerController(picker: UIImagePickerController
    , didFinishPickingMediaWithInfo info: [String : AnyObject]) {
    // The info dictionary contains multiple representations of the image, and this uses the original.
    let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
    let url = info[UIImagePickerControllerReferenceURL] as! NSURL

    // Set photoImageView to display the selected image.
    photoImageView.image = selectedImage
    let fetchResult = PHAsset.fetchAssetsWithALAssetURLs([url], options: nil)

    let asset = fetchResult.firstObject as? PHAsset
    let location = asset?.location

    // Dismiss the picker.
    dismissViewControllerAnimated(true, completion: nil)
  }

  //MARK: Actions
  @IBAction func selectImageFromPhotoLibrary(sender: UITapGestureRecognizer) {

    // UIImagePickerController is a view controller that lets a user pick media from their photo library.
    let imagePickerController = UIImagePickerController()

    // Only allow photos to be picked, not taken.
    imagePickerController.sourceType = .PhotoLibrary

    // Make sure ViewController is notified when the user picks an image.
    imagePickerController.delegate = self

    presentViewController(imagePickerController, animated: true, completion: nil)
  }

  @IBAction override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

    guard let sender = sender where sender === saveButton else {
      return
    }

    photo = Photo(name: name, photo: photoImageView.image!
      , rating: ratingControl.rating, latitude: latitude, longitude: longitude)
  }

  @IBAction func cancel(sender: UIBarButtonItem) {
    dismissViewControllerAnimated(true, completion: nil)
  }
}

