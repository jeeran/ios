//
//  Photo.swift
//  GeoPhotos
//
//  Created by Shaikh Amjad on 14/01/2016.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit

class Photo {

  //MARK: Properties
  var name:String
  var photo:UIImage
  var rating: Int
  var latitude: NSNumber?
  var longitude: NSNumber?

  //MARK: Init
  init(name: String, photo: UIImage, rating: Int, latitude: NSNumber, longitude: NSNumber) {
    // Initialize stored properties.
    self.name = name
    self.photo = photo
    self.rating = rating
    self.latitude = latitude
    self.longitude = longitude
  }

}
