//
//  VCMapView.swift
//  HonoluluArt
//
//  Created by Shaikh Amjad on 23/12/2015.
//  Copyright © 2015 Home. All rights reserved.
//

//import Foundation
import UIKit
import MapKit

extension ViewController: MKMapViewDelegate
, UIImagePickerControllerDelegate
, UINavigationControllerDelegate {

  // 1
  func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation)
    -> MKAnnotationView? {

    if let annotation = annotation as? Artwork {
      let identifier = "pin"
      var view: MKPinAnnotationView

      if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
        as? MKPinAnnotationView { // 2
          dequeuedView.annotation = annotation
          view = dequeuedView
      } else {
        // 3
        view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        view.canShowCallout = true
        view.calloutOffset = CGPoint(x: -5, y: 5)
        view.rightCalloutAccessoryView = UIButton(type:UIButtonType.DetailDisclosure) as UIView
      }

      return view
    }
    return nil
  }


  func mapView(mapView: MKMapView, annotationView view: MKAnnotationView,
    calloutAccessoryControlTapped control: UIControl) {

      let storyboard = UIStoryboard(name: "Main", bundle:nil)
      let imageController = storyboard.instantiateViewControllerWithIdentifier("ImageViewController")
        as! ImageViewController

      showDetailViewController(imageController, sender: self)
  }
}
