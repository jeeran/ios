//
//  ViewController.swift
//  HonoluluArt
//
//  Created by Shaikh Amjad on 23/12/2015.
//  Copyright © 2015 Home. All rights reserved.
//

import MapKit

class ViewController: UIViewController {

  //MARK: - Outlets
  @IBOutlet var mapView: MKMapView!

  //MARK: - Properties
  let regionRadius: CLLocationDistance = 1000
  // var imageController : ImageViewController

  //MARK: - Methods
  func centerMapOnLocation(location: CLLocation) {
    //
    let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
      regionRadius * 2.0, regionRadius * 2.0)

    mapView.setRegion(coordinateRegion, animated: true)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    mapView.delegate = self

    // set initial location in Honolulu
    let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
    centerMapOnLocation(initialLocation)

    // show artwork on map
    let artwork = Artwork(title: "King David Kalakaua",
      locationName: "Waikiki Gateway Park",
      discipline: "Sculpture",
      coordinate: CLLocationCoordinate2D(latitude: 21.283921, longitude: -157.831661))

    mapView.addAnnotation(artwork)
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

