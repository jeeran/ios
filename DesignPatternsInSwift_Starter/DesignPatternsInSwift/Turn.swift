//
//  Turn.swift
//  DesignPatternsInSwift
//
//  Created by Shaikh Amjad on 27/12/2015.
//  Copyright © 2015 RepublicOfApps, LLC. All rights reserved.
//

import Foundation

class Turn {
  // 1
  let shapes: [Shape]
  var matched: Bool?

  init(shapes: [Shape]) {
    self.shapes = shapes
  }

  // 2
  func turnCompletedWithTappedShape(tappedShape: Shape) {
    var maxArea = shapes.reduce(0) { $0 > $1.area ? $0 : $1.area }
    matched = tappedShape.area >= maxArea
  }
}
