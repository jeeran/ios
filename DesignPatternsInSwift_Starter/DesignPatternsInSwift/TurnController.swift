//
//  TurnController.swift
//  DesignPatternsInSwift
//
//  Created by Shaikh Amjad on 27/12/2015.
//  Copyright © 2015 RepublicOfApps, LLC. All rights reserved.
//

import Foundation

class TurnController {
  var currentTurn: Turn?
  var pastTurns: [Turn] = [Turn]()

  // 1
  init(turnStrategy: TurnStrategy) {
    self.turnStrategy = turnStrategy
    self.scorer = MatchScorer()

    self.scorer.nextScorer = StreakScorer()

  }

  func beginNewTurn() -> (ShapeView, ShapeView) {
    // 2
    let shapeViews = turnStrategy.makeShapeViewsForNextTurnGivenPastTurns(pastTurns)
    currentTurn = Turn(shapes: [shapeViews.0.shape, shapeViews.1.shape])
    return shapeViews
  }

  func endTurnWithTappedShape(tappedShape: Shape) -> Int {
    currentTurn!.turnCompletedWithTappedShape(tappedShape)
    pastTurns.append(currentTurn!)

    let scoreIncrement = scorer.computeScoreIncrement(pastTurns.reverse())
    return scoreIncrement
  }

  private let turnStrategy: TurnStrategy
  private let scorer: Scorer
}