//
//  AlbumExtensions.swift
//  BlueLibrarySwift
//
//  Created by Shaikh Amjad on 01/08/2015.
//  Copyright © 2015 Raywenderlich. All rights reserved.
//

import Foundation

extension Album {
    func ae_tableRepresentation() -> (titles:[String], values:[String]) {
        return (["Artist", "Album", "Genre", "Year"], [artist, title, genre, year])
    }
}
