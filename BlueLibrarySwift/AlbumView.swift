//
//  AlbumView.swift
//  BlueLibrarySwift
//
//  Created by Shaikh Amjad on 01/08/2015.
//  Copyright © 2015 Raywenderlich. All rights reserved.
//

import UIKit

class AlbumView: UIView {

    //MARK: Properties
    private var coverImage: UIImageView!
    private var indicator: UIActivityIndicatorView!

    //MARK: Inits
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    init(frame: CGRect, albumCover: String) {
        super.init(frame: frame)
        commonInit()

        NSNotificationCenter.defaultCenter().postNotificationName("BLDownloadImageNotification"
            , object: self, userInfo: ["imageView":coverImage, "coverUrl" : albumCover])
    }

    deinit {
        coverImage.removeObserver(self, forKeyPath: "image")
    }

    //MARK: Overrides

    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?
        , change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
            if keyPath == "image" {
                indicator.stopAnimating()
            }
    }


    //MARK: Methods
    func commonInit() {
        backgroundColor = UIColor.blackColor()
        coverImage = UIImageView(frame: CGRect(x: 5, y: 5
            , width: frame.size.width - 10, height: frame.size.height - 10))

        let options : NSKeyValueObservingOptions = .New //NSKeyValueObservingOptions([.New, .Old])


        addSubview(coverImage)
        coverImage.addObserver(self, forKeyPath:"image", options:options, context:nil)

        indicator = UIActivityIndicatorView()
        indicator.center = center

        indicator.activityIndicatorViewStyle = .WhiteLarge
        indicator.startAnimating()

        addSubview(indicator)
    }

    func highlightAlbum(didHighlightView: Bool) {
        if didHighlightView == true {
            backgroundColor = UIColor.whiteColor()
        } else {
            backgroundColor = UIColor.blackColor()
        }
    }

}
